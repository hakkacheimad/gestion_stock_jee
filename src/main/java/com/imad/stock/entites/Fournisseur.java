package com.imad.stock.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Fournisseur implements Serializable {
	
	@Id
	@GeneratedValue	
	private Long idFournisseur;
		
	private String nom;
	
	private String prenom;
	
	private String adresse;
	
	@OneToMany(mappedBy = "fournisseur")
	private List<CommandeFournisseur> cmdfournisseurs;
	
	public Fournisseur() {
		super();
	}

	public Long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(Long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<CommandeFournisseur> getCmdfournisseurs() {
		return cmdfournisseurs;
	}

	public void setCmdfournisseurs(List<CommandeFournisseur> cmdfournisseurs) {
		this.cmdfournisseurs = cmdfournisseurs;
	}


	
	

}
