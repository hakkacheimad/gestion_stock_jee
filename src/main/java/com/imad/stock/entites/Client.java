package com.imad.stock.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Client implements Serializable {
	
	@Id
	@GeneratedValue	
	private Long idClient;
	
	private String nom;
	
	private String prenom;
	
	private String adresse;
	
	private String Mail;
	
	@OneToMany(mappedBy = "client")
	private List<CommandeClient> CommandeClients;
	
	
	public Client() {
		
	}

	public List<CommandeClient> getCommandeClients() {
		return CommandeClients;
	}

	public void setCommandeClients(List<CommandeClient> commandeClients) {
		CommandeClients = commandeClients;
	}

	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getMail() {
		return Mail;
	}

	public void setMail(String mail) {
		Mail = mail;
	}

	public Long getIdCategory() {
		return idClient;
	}

	public void setIdCategory(Long idCategory) {
		this.idClient = idCategory;
	}
	
	

}
