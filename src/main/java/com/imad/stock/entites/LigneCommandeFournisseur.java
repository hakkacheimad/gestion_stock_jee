package com.imad.stock.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class LigneCommandeFournisseur implements Serializable {
	
	@Id
	@GeneratedValue	
	private Long idLcdefour;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name = "idcmdfournissur")
	private CommandeFournisseur cmdfournisseur;

	public Long getIdLcdefour() {
		return idLcdefour;
	}

	public void setIdLcdefour(Long idLcdefour) {
		this.idLcdefour = idLcdefour;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCmdfournisseur() {
		return cmdfournisseur;
	}

	public void setCmdfournisseur(CommandeFournisseur cmdfournisseur) {
		this.cmdfournisseur = cmdfournisseur;
	}
	
	

}
