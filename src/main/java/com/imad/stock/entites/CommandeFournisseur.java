package com.imad.stock.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class CommandeFournisseur implements Serializable {
	
	@Id
	@GeneratedValue	
	private Long idcmdfournissur;
	
	@ManyToOne
	@JoinColumn(name = "idFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy = "cmdfournisseur")
	private List<LigneCommandeFournisseur> LigneCommandefournissurs;

	public Long getIdcmdfournissur() {
		return idcmdfournissur;
	}

	public void setIdcmdfournissur(Long idcmdfournissur) {
		this.idcmdfournissur = idcmdfournissur;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getLigneCommandefournissurs() {
		return LigneCommandefournissurs;
	}

	public void setLigneCommandefournissurs(List<LigneCommandeFournisseur> ligneCommandefournissurs) {
		LigneCommandefournissurs = ligneCommandefournissurs;
	}


	

}
