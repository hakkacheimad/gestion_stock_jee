package com.imad.stock.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class LigneCommandeCLient implements Serializable {
	
	@Id
	@GeneratedValue	
	private Long idLcdeclt;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name = "idCommandeClient")
	private CommandeClient cmdclient;

	public Long getIdCategory() {
		return idLcdeclt;
	}

	public Long getIdLcdeclt() {
		return idLcdeclt;
	}

	public void setIdLcdeclt(Long idLcdeclt) {
		this.idLcdeclt = idLcdeclt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCmdclient() {
		return cmdclient;
	}

	public void setCmdclient(CommandeClient cmdclient) {
		this.cmdclient = cmdclient;
	}

	public void setIdCategory(Long idCategory) {
		this.idLcdeclt = idCategory;
	}
	
	

}
