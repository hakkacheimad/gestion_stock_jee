package com.imad.stock.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable {
	
	@Id
	@GeneratedValue	
	private Long idVente;
	
	private String Code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datevente;

	@OneToMany(mappedBy = "vente")
    private List<LigneVente> lignesventes;

	public Long getIdVente() {
		return idVente;
	}

	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public Date getDatevente() {
		return datevente;
	}

	public void setDatevente(Date datevente) {
		this.datevente = datevente;
	}

	public List<LigneVente> getLignesventes() {
		return lignesventes;
	}

	public void setLignesventes(List<LigneVente> lignesventes) {
		this.lignesventes = lignesventes;
	}
	

	
}
