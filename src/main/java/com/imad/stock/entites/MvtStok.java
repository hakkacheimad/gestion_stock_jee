package com.imad.stock.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStok implements Serializable {
	
	public static final int ENTRE = 1;
	public static final int SORTIE = 2;
	@Id
	@GeneratedValue	
	private Long idmvtstock;
 
	@Temporal(TemporalType.TIMESTAMP)
	private Date datemvt;
	
	private BigDecimal quatite;
	
	private int typemvt;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
    private Article article;

	public Long getIdmvtstock() {
		return idmvtstock;
	}

	public void setIdmvtstock(Long idmvtstock) {
		this.idmvtstock = idmvtstock;
	}

	public Date getDatemvt() {
		return datemvt;
	}

	public void setDatemvt(Date datemvt) {
		this.datemvt = datemvt;
	}

	public BigDecimal getQuatite() {
		return quatite;
	}

	public void setQuatite(BigDecimal quatite) {
		this.quatite = quatite;
	}

	public int getTypemvt() {
		return typemvt;
	}

	public void setTypemvt(int typemvt) {
		this.typemvt = typemvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
	

}
