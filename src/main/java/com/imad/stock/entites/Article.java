package com.imad.stock.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;



@Entity
@Table(name = "article")
public class Article implements Serializable{

	@Id
	@GeneratedValue
	private Long idArticle;
	
	private String CodeArticle;
	
	private String designation;
	
	private BigDecimal PrixUnitaireHT;
	
	private BigDecimal tauxTva;
	
	private BigDecimal PrixUnitaireTTC;
	
	private String photo;
	
	
	
	public Article() {
          super();
	}

	@ManyToOne
	@JoinColumn(name = "idCategory")
	private Categery categry;
	
	

	public String getCodeArticle() {
		return CodeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		CodeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaireHT() {
		return PrixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		PrixUnitaireHT = prixUnitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaireTTC() {
		return PrixUnitaireTTC;
	}

	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		PrixUnitaireTTC = prixUnitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categery getCategry() {
		return categry;
	}

	public void setCategry(Categery categry) {
		this.categry = categry;
	}

	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
	
}
